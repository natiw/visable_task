# frozen_string_literal: true

require "rails_helper"
RSpec.describe AccountsService do
  describe "#send_to" do
    context "with a invalid account number" do
      subject { create(:account) }
      let(:amount) { 0.5 * subject.account_balance.to_i }

      it "fails" do
        expect { described_class.send_to subject.id, "", amount }.to raise_exception ActiveRecord::RecordNotFound
      end
    end

    context "with a invalid transfer amount" do
      subject { create(:account) }
      let(:reciever) { create(:account) }

      it "fails" do
        expect { described_class.send_to subject.id, reciever.account_number, 0 }.to raise_exception ArgumentError
      end
    end

    context "with transfer amount greater than account balance" do
      subject { create(:account) }
      let(:reciever) { create(:account) }
      let(:amount) { subject.account_balance.to_i + 1 }

      it "fails" do
        expect { described_class.send_to subject.id, reciever.account_number, amount }.to raise_exception ArgumentError
      end
    end
    context "with reciever same as sender" do
      subject { create(:account) }
      let(:reciever) { create(:account) }
      let(:amount) { subject.account_balance.to_i + 1 }

      it "fails" do
        expect { described_class.send_to subject.id, subject.account_number, amount }.to raise_exception ArgumentError
      end
    end
    context "with a valid account number and transfer amount" do
      subject { create(:account) }
      let(:reciever) { create(:account) }
      let(:amount) { (0.5 * subject.account_balance.to_i).to_i }

      it "succeeds" do
        expect { described_class.send_to subject.id, reciever.account_number, amount }.not_to raise_exception
      end
    end
  end

  describe "#fetch_last_n_transactions_for" do
    context "with invalid account id" do
      it "raises an exception" do
        expect {
          described_class.fetch_last_n_transactions_for("")
        }.to raise_exception
      end
    end
    context "with valid account id" do
      subject { create(:account) }
      let(:reciever) { create(:account) }
      let(:amount) { (0.5 * subject.account_balance.to_i).to_i }

      before do
        described_class.send_to subject.id, reciever.account_number, amount
      end

      it "succeeds" do
        expect(subject.transactions.count).to be 1
      end
    end
  end
end
