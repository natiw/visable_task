# frozen_string_literal: true

FactoryBot.define do
  factory :account do
    holder { Faker::Name.name }
    balance { Random.new.rand(500..10000) }
    account_number { Faker::Bank.account_number }
  end

  # factory :account do
  #   holder { Faker::Name.name }
  #   balance { Faker::Number.decimal(l_digits: 3, r_digits: 2) }
  #   account_number { Faker::Bank.account_number }
  # end
end
