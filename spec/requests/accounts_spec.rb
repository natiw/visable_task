# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Accounts API", type: :request do
  describe "GET /api/v1/accounts" do
    it "succeeds!" do
       headers = {
         "ACCEPT" => "application/json",     # This is what Rails 4 accepts
         "HTTP_ACCEPT" => "application/json" # This is what Rails 3 accepts
       }
       get "/api/v1/accounts", headers: headers
       expect(response).to have_http_status(200)
       expect(json).to be_empty
     end
  end

  describe "GET /api/v1/accounts/:id" do
    subject { create(:account) }
    it "succeeds" do
        headers = {
            "ACCEPT" => "application/json",     # This is what Rails 4 accepts
            "HTTP_ACCEPT" => "application/json" # This is what Rails 3 accepts
          }
        get "/api/v1/accounts", params: { id: subject.id }, headers: headers
        expect(response).to have_http_status(200)
        expect(json).not_to be_empty

        match_response_schema "account"
      end
  end

  describe "POST /api/v1/accounts" do
    let(:post_params) { attributes_for(:account) }
    it "succeeds" do
        post "/api/v1/accounts", params: { account: post_params }
        expect(response).to have_http_status(201)
        match_response_schema "account"
      end
  end

  describe "DELETE /api/v1/accounts/:id" do
    subject { create(:account) }
    it "succeeds" do
      delete "/api/v1/accounts/#{subject.id}"
      expect(response).to have_http_status(204)
    end
  end

  describe "POST /api/v1/accounts/:id/transfer" do
    let(:sender) { create(:account) }
    let(:reciever) { create(:account) }

    let (:post_data) do
      {
         "reciever": reciever.account_number.to_s,
         "amount": (0.5 * sender.account_balance.to_i).to_i
       }
    end

    it "succeeds" do
      headers = {
         "ACCEPT" => "application/json",     # This is what Rails 4 accepts
         "HTTP_ACCEPT" => "application/json" # This is what Rails 3 accepts
       }
      post "/api/v1/accounts/#{sender.id}/transfer", params: { details: post_data }, headers: headers
      expect(response).to have_http_status(200)
    end
  end
end
