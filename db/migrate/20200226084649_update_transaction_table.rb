class UpdateTransactionTable < ActiveRecord::Migration[5.2]
	def change
		change_table :transactions do |t|
			t.belongs_to :account
			t.integer :transaction_status, default: 0, index: true
			# Most use case is that we want to get transactions by sender
			# hence, we need an index on sender
			t.index :sender
			# We need index here later on for fetching transaction
			# history
			t.index :reciever
		end
  end
end
