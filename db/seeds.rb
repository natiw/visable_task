# frozen_string_literal: true

2.times do |i|
  account_number = "%010d" % rand(10 ** 8)
  balance = Money.new(100000, "EUR")
  Account.new(holder: "Johnny Cash #{i}", account_balance: balance, account_number: account_number).save
end

6.times do |i|
  # find accounts
  a_sender = Account.first
  a_reciever = Account.second

  # instantiate transaction
  amount = (10 * a_sender.account_balance.to_i) # 50% of the fractional amount
  t = a_sender.transactions.create(reciever: a_reciever.account_number, amount: amount)

  # reduce sender balance
  a_sender.account_balance = a_sender.account_balance - Money.from_amount(amount)
  a_sender.save

  # increment reciever balance
  a_reciever.account_balance = a_reciever.account_balance + t.transfer_amount
  a_reciever.save

  # mark transaction as done
  t.transferred!
end

5.times do |i|
  # find accounts
  a_sender = Account.second
  a_reciever = Account.first

  # instantiate transaction
  amount = (10 * a_sender.account_balance.to_i) # 50% of the fractional amount
  t = a_sender.transactions.create(reciever: a_reciever.account_number, amount: amount)

  # reduce sender balance
  a_sender.account_balance = a_sender.account_balance - Money.from_amount(amount)
  a_sender.save

  # increment reciever balance
  a_reciever.account_balance = a_reciever.account_balance + t.transfer_amount
  a_reciever.save

  # mark transaction as done
  t.transferred!
end
