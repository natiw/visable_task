if @error_class == "ActiveRecord::RecordNotFound"
	json.partials "record_not_found"
elsif @error_class == "ArgumentError"
	json.partials "argument_error", message: @message
else
	json.message @message[:message]
	json.status "Failed"
end
