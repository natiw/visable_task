json.account do
	json.id account.id
	json.account_holder account.holder
	json.account_number account.account_number
	json.account_balance account.account_balance.format
	# json.transactions account.get_last_n_transactions, partial: 'transactions', as: :transaction, locals: {account: account}
	json.transactions AccountsService.fetch_last_n_transactions_for(account.id),
						partial: 'transactions',
						as: :transaction,
						locals: {account: account}
end
