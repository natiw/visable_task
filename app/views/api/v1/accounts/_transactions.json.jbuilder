json.amount transaction.transfer_amount.format
json.transfer_date transaction.created_at
if transaction.reciever != account.account_number
	json.set! :transaction_type, 'Outgoing'
	json.set! :sent_to, transaction.reciever
else
	json.set! :transaction_type, 'Incoming'
	json.set! :recieved_from, Account.find_by(id: transaction.account_id).account_number
end

