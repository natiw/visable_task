if @account
	json.partial! "account", account: @account
else
	json.message "Account doesn't exist"
end
