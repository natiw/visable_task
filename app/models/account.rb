# frozen_string_literal: true

class Account < ApplicationRecord
  has_many :transactions

  monetize :balance, as: "account_balance", allow_nil: false, numericality: {
    greater_than_or_equal_to: 0,
  }
  enum status: [ :pending, :transferred, :cancelled ]
  validates_presence_of :holder
  validates_presence_of :balance
  validates_presence_of :account_number

  # Only for new accounts
  before_validation :set_account_number

  def set_account_number
    self.account_number = self.account_number || "%010d" % rand(10 ** 8)
  end

  def send_to(reciever_account_number, amount)
    transfer_amount = amount.instance_of?(Money) ? amount : Money.from_amount(amount)
    if self.account_balance < transfer_amount
      raise ArgumentError.new("Account doesn't have sufficient balance.")
    else
      self.account_balance = self.account_balance - transfer_amount
      begin
        self.transactions.create(reciever: reciever_account_number, amount: amount * 100) # Money#from_amount doesn't work here
        self.save
        AccountTransferJob.perform_later self.id
      rescue => exception
        puts exception
        raise ArgumentError.new("Server error")
      end
    end
  end
end
