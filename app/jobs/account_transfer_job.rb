# frozen_string_literal: true

class AccountTransferJob < ApplicationJob
  queue_as :default

  def perform(sender_id)
    t = Transaction.find_by!(account_id: sender_id, transaction_status: :pending)
    # set status to transferred
    t.transferred!
    reciever = Account.find_by!(account_number: t.reciever)
    reciever.account_balance = reciever.account_balance + t.transfer_amount
    reciever.save
  end
end
