# frozen_string_literal: true

module Api
  module V1
    class AccountsController < ApplicationController
      before_action :set_account, only: [:show, :edit, :update, :destroy, :transfer, :overview]
      # GET /accounts
      # GET /accounts.json
      def index
        @accounts = Account.all
      end

      # GET /accounts/1
      # GET /accounts/1.json
      def show
        render :show
      end

      # POST /accounts
      # POST /accounts.json
      def create
        @account = Account.new(new_account_params)

        if @account.save
          render json: @account, status: :created
        else
          render json: @account.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /accounts/1
      # PATCH/PUT /accounts/1.json
      def update
        if @account.update(update_account_params)
          render json: @account, status: :ok, location: @account
        else
          render json: @account.errors, status: :unprocessable_entity
        end
      end

      # DELETE /accounts/1
      # DELETE /accounts/1.json
      def destroy
        @account.destroy
        head :no_content
      end

      # GET /accounts/1/overview
      def overview
       end

      # POST /accounts/1/transfer
      def transfer
        AccountsService.send_to(
          params[:id],
          transfer_params[:reciever],
          transfer_params[:amount])

      rescue Exception => e
        @error_class = e.class.to_s
        @message = e.to_s
        if @error_class == "ActiveRecord::RecordNotFound"
          render json: { error: "Reciever account doesn't exist" }, status: :bad_request
        else
          render json: { error: @message }, status: :bad_request
        end
      else
        render "transfer_success", status: :ok
       end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_account
          @account = AccountsService.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def account_params
          params.require(:account).permit(:holder, :account_number, :balance)
          end

        def update_account_params
          params.permit(:holder)
         end

        def transfer_params
          params.require(:details).permit(:reciever, :amount)
         end
        def new_account_params
          account_params.permit(:holder, :balance)
         end

        def update_account_params
          account_params.permit(:holder, :balance)
         end
    end
  end
end
