# frozen_string_literal: true

require "sidekiq/web"
Rails.application.routes.draw do
  mount Sidekiq::Web => "/sidekiq", :as => "sidekiq"
  namespace "api" do
    namespace "v1" do
      resources :accounts
      get "/accounts/:id/overview", to: "accounts#overview"
      post "/accounts/:id/transfer", to: "accounts#transfer"
    end
  end
end
